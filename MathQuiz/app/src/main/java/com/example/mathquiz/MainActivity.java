package com.example.mathquiz;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.Collections;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity {
    Button start;

    Button ans1;
    Button ans2;
    Button ans3;
    Button ans4;

    TextView points;
    TextView problem;
    TextView time;

    int score = 0;

    int currentAnswer = 0;

    ProgressBar pg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.start = findViewById(R.id.start);

        this.ans1 = findViewById(R.id.ans1);
        this.ans2 = findViewById(R.id.ans2);
        this.ans3 = findViewById(R.id.ans3);
        this.ans4 = findViewById(R.id.ans4);

        this.ans1.setClickable(false);
        this.ans2.setClickable(false);
        this.ans3.setClickable(false);
        this.ans4.setClickable(false);

        this.points = findViewById(R.id.score);
        this.problem = findViewById(R.id.questions);
        this.time = findViewById(R.id.time);

        this.pg = findViewById(R.id.progressBar);
    }

    public void start(View v) {
        start.setVisibility(View.GONE);
        start.setClickable(false);

        ans1.setClickable(true);
        ans2.setClickable(true);
        ans3.setClickable(true);
        ans4.setClickable(true);

        final int seconds = 25;
        pg.setMax(seconds);
        pg.setMin(0);

        getQuestion();

        Handler handler = new Handler();

        class updatePG implements Runnable {
            int seconds;

            updatePG(int seconds) {
                this.seconds = seconds;
            }

            public void run() {
                pg.setProgress(this.seconds, true);
            }
        }

        for (int i = seconds; i > 0; i--) {
            handler.postDelayed(new updatePG(i), 1000);
        }

        start.setVisibility(View.VISIBLE);
        start.setClickable(true);

        ans1.setClickable(false);
        ans2.setClickable(false);
        ans3.setClickable(false);
        ans4.setClickable(false);
    }

    public void ans1_click(View v) {
        if (ans1.getText() == Integer.toString(currentAnswer)) {
            score += 10;

            getQuestion();
        }

        else {
            score -= 5;

            getQuestion();
        }
    }

    public void ans2_click(View v) {
        if (ans2.getText() == Integer.toString(currentAnswer)) {
            score += 10;

            getQuestion();
        }

        else {
            score -= 5;

            getQuestion();
        }
    }

    public void ans3_click(View v) {
        if (ans3.getText() == Integer.toString(currentAnswer)) {
            score += 10;

            getQuestion();
        }

        else {
            score -= 5;

            getQuestion();
        }
    }

    public void ans4_click(View v) {
        if (ans4.getText() == Integer.toString(currentAnswer)) {
            score += 10;

            getQuestion();
        }

        else {
            score -= 5;

            getQuestion();
        }
    }

    // Fisher-Yates shuffle
    static void shuffleArray(int[] ar)
    {
        Random rand = new Random();
        for (int i = ar.length - 1; i > 0; i--)
        {
            int index = rand.nextInt(i + 1);
            // Simple swap
            int a = ar[index];
            ar[index] = ar[i];
            ar[i] = a;
        }
    }

    private void getQuestion() {
        Random rand = new Random();

        char[] operators = {'+', '-', '*'};

        int partA;
        int partB;

        int wrongA = 0;
        int wrongB = 0;

        int opNum;

        int answer = 0;
        int wrongAns1 = 0;
        int wrongAns2 = 0;
        int wrongAns3 = 0;

        int[] answers = new int[4];

        partA = rand.nextInt(20) + 1;
        partB = rand.nextInt(20) + 1;

        while (wrongA != partB)
            wrongA = partA - (rand.nextInt(partA) + 1) / 2;

        while (wrongB != partA)
            wrongB = partB - (rand.nextInt(partB) + 1) / 2;

        opNum = rand.nextInt(operators.length);

        switch (operators[opNum]) {
            case '+': {
                answer = partA + partB;

                wrongAns1 = partA + wrongA;
                wrongAns2 = partA + wrongB;
                wrongAns3 = partB + wrongA;
            }

            case '-': {
                answer = partA - partB;

                wrongAns1 = partA - wrongA;
                wrongAns2 = partA - wrongB;
                wrongAns3 = partB - wrongA;
            }

            case '*': {
                answer = partA * partB;

                wrongAns1 = partA * wrongA;
                wrongAns2 = partA * wrongB;
                wrongAns3 = partB * wrongA;
            }
        }

        currentAnswer = answer;

        answers = new int [] {answer, wrongAns1, wrongAns2, wrongAns3};
        shuffleArray(answers);

        problem.setText(String.format("%d%c%d", partA, operators[opNum], partB));

        ans1.setText(answers[0]);
        ans2.setText(answers[1]);
        ans3.setText(answers[2]);
        ans4.setText(answers[3]);
    }
}