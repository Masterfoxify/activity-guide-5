package com.example.diceroller;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import java.util.Random;

public class MainActivity extends AppCompatActivity {
    ImageView die;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        die = findViewById(R.id.die);
    }

    public void click(View v) {
        Random rand = new Random();

        int[] rollImages = {R.drawable.die1, R.drawable.die2, R.drawable.die3,
                            R.drawable.die4, R.drawable.die5, R.drawable.die6};

        int roll = rand.nextInt(6);

        this.die.setImageResource(rollImages[roll]);
    }
}
