package com.example.clickergame;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {
    Button click_1;
    Button click_2;

    final float click_1X = 430f;
    final float click_1Y = 600f;

    final float click_2X = 430f;
    final float click_2Y = 960f;

    TextView clicks;

    boolean click1 = true;
    boolean click2 = true;
    int clickCount = 0;

    boolean counting = false;

    Timer timer = new Timer();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.click_1 = findViewById(R.id.click_1);
        this.click_2 = findViewById(R.id.click_2);

        this.clicks = findViewById(R.id.clicks);
    }

    public static float dipToPixels(View v, float dipValue){
        DisplayMetrics metrics = v.getResources().getDisplayMetrics();

        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,  dipValue, metrics);
    }

    public void click1(View v) {
        if (this.click1 || !this.counting) {
            v.setBackgroundColor(Color.GRAY);
            v.setClickable(false);

            this.click2 = true;
            this.click1 = false;

            this.click_2.setClickable(click2);
            this.click_2.setBackgroundColor(Color.BLACK);

            Random rand = new Random();

            this.click_1.setX(dipToPixels(v, 250) * rand.nextFloat());
            this.click_1.setY(dipToPixels(v, 100) * rand.nextFloat() + 400);

            this.clickCount++;

            this.clicks.setText("Clicks: " + Integer.toString(clickCount));

            if (!this.counting) {
                this.counting = true;

                this.timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (counting) {
                                    counting = false;

                                    click_1.setX(click_1X);
                                    click_1.setY(click_1Y);
                                    click_1.setBackgroundColor(Color.BLACK);
                                    click1 = true;

                                    click_2.setX(click_2X);
                                    click_2.setY(click_2Y);
                                    click_2.setBackgroundColor(Color.BLACK);
                                    click2 = true;

                                    clickCount = 0;
                                }
                            }
                        });
                    }
                }, 10000);
            }
        }
    }

    public void click2(View v) {
        if (this.click2 || !this.counting) {
            v.setBackgroundColor(Color.GRAY);
            v.setClickable(false);

            this.click1 = true;
            this.click2 = false;

            this.click_1.setClickable(click1);
            this.click_1.setBackgroundColor(Color.BLACK);

            Random rand = new Random();

            this.click_2.setX(dipToPixels(v, 250) * rand.nextFloat());
            this.click_2.setY(dipToPixels(v, 200) * rand.nextFloat() + 850);

            this.clickCount++;

            this.clicks.setText("Clicks: " + Integer.toString(clickCount));

            if (!this.counting) {
                this.counting = true;

                this.timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (counting) {
                                    counting = false;

                                    click_1.setX(click_1X);
                                    click_1.setY(click_1Y);
                                    click_1.setBackgroundColor(Color.BLACK);
                                    click1 = true;

                                    click_2.setX(click_2X);
                                    click_2.setY(click_2Y);
                                    click_2.setBackgroundColor(Color.BLACK);
                                    click2 = true;

                                    clickCount = 0;
                                }
                            }
                        });
                    }
                }, 10000);
            }
        }
    }
}
